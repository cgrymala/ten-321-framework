<?php global $ten321; ?>
<?php if ( ! array_key_exists( 'above-header', $ten321->get_default_sidebars() ) ) { return; } ?>
<?php
if( is_active_sidebar('above-header') ) {
?>
<aside class="sidebar above-header" role="complementary">
	<ul class="widget-area">
    	<?php dynamic_sidebar('above-header') ?>
    </ul>
</aside>
<?php
}
?>