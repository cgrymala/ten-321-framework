<?php global $ten321; ?>
<?php do_action( 'ten321-before-loop' ) ?>
<?php
if( have_posts() ) : while( have_posts() ) : the_post();
?>
<article <?php post_class( apply_filters( 'ten321-post-class', '' ) ) ?>>
	<?php do_action( 'ten321-before-title' ) ?>
	<h1><?php echo is_singular() ? apply_filters( 'the_title', get_the_title(), get_the_ID() ) : '<a href="' . apply_filters( 'the_permalink', get_permalink() ) . '" title="' . apply_filters( 'the_title_attribute', get_the_title(), get_the_ID() ) . '">' . apply_filters( 'the_title', get_the_title(), get_the_ID() ) . '</a>' ?></h1>
    <?php do_action( 'ten321-after-title' ) ?>
    <section class="post-content">
    	<?php do_action( 'ten321-before-content' ) ?>
    	<?php the_content( apply_filters( 'ten321-loop-read-more-text', 'Read more...', $post ) ) ?>
        <?php do_action( 'ten321-after-content' ) ?>
    </section>
<?php
if( 'open' == $post->comment_status ) {
	do_action( 'ten321-before-comments' );
	comments_template();
	do_action( 'ten321-after-comments' );
}
?>
</article>
<?php
endwhile; else :
?>
<article class="not-found">
	<?php do_action( 'ten321-before-title' ) ?>
	<h1><?php _e( 'Not Found', $ten321->text_domain ) ?></h1>
    <?php do_action( 'ten321-after-title' ) ?>
    <section class="content">
    	<?php do_action( 'ten321-before-content' ) ?>
    	<p><?php _e( 'Unfortunately, no results were found that matched the criteria specified. Please try again later.', $ten321->text_domain ) ?></p>
        <?php do_action( 'ten321-after-content' ) ?>
    </section>
</article>
<?php endif; ?>
<?php do_action( 'ten321-after-loop' ) ?>