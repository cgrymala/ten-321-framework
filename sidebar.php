<?php global $ten321; ?>
<?php if ( ! array_key_exists( 'primary', $ten321->get_default_sidebars() ) ) { return; } ?>
<?php if( is_active_sidebar('primary') || apply_filters( 'ten321_has_nav_menu', has_nav_menu( 'sidebar' ), 'sidebar' ) || apply_filters( 'ten321_has_nav_menu', has_nav_menu( 'side-second' ), 'side-second' ) ) : ?>

<aside class="sidebar primary-sidebar column" role="complementary">
<?php get_template_part( 'nav', 'sidebar' ) ?>
<?php
	if( is_active_sidebar('primary') ) {
?>
	<ul class="widget-area">
    	<?php dynamic_sidebar('primary') ?>
    </ul>
<?php
	}
?>
</aside>

<?php endif; ?>