<?php
/*
Plugin Name: Ten-321 Navigation Widget
Plugin URI: http://plugins.ten-321.com/navigation-widget/
Description: Allows you to insert a context-aware navigation menu as a widget
Version: 0.1a
Author: cgrymala
Author URI: http://ten-321.com/
License: GPL2
*/
class ten321_navigation_menu extends WP_Widget {
	function __construct() {
		$widget_ops = array( 'classname' => 'ten321-navigation', 'description' => __( 'Shows the standard WordPress navigation menu' ) );
		$control_ops = array( 'id_base' => 'ten321-navigation' );
		parent::__construct( 'ten321-navigation', __( 'Page Menu' ), $widget_ops, $control_ops );
	}
	
	function get_defaults() {
		return array(
			'title'             => null, 
			'no_child_behavior' => '', 
			'levels'            => 0, 
		);
	}
	
	function form( $instance ) {
		$instance = wp_parse_args( $instance, $this->get_defaults() );
?>
<p><label for="<?php echo $this->get_field_id( 'title' ) ?>"><?php _e( 'Title' ) ?></label>
	<input type="text" name="<?php echo $this->get_field_name( 'title' ) ?>" id="<?php echo $this->get_field_id( 'title' ) ?>" value="<?php echo $instance['title'] ?>" class="widefat"/></p>
<p><label for="<?php echo $this->get_field_id( 'no_child_behavior' ) ?>"><?php _e( 'If the top-level page does not have any children, what should be displayed in the menu?' ) ?></label>
	<select class="widefat" name="<?php echo $this->get_field_name( 'no_child_behavior' ) ?>" id="<?php echo $this->get_field_id( 'no_child_behavior' ) ?>">
    	<option value=""<?php selected( $instance['no_child_behavior'], '' ) ?>><?php _e( 'Nothing' ) ?></option>
        <option value="current"<?php selected( $instance['no_child_behavior'], 'current' ) ?>><?php _e( 'Just the current page' ) ?></option>
        <option value="top"<?php selected( $instance['no_child_behavior'], 'top' ) ?>><?php _e( 'All top-level pages' ) ?></option>
    </select></p>
<p><label for="<?php echo $this->get_field_id( 'levels' ) ?>"><?php _e( 'How many levels of navigation should be shown?' ) ?></label>
	<input type="text" name="<?php echo $this->get_field_name( 'levels' ) ?>" id="<?php echo $this->get_field_id( 'levels' ) ?>" value="<?php echo $instance['levels'] ?>"/>
    <br/><span style="font-style: italic"><?php _e( 'If set to 0, all available levels will be included.' ) ?></p>
<?php
	}
	
	function update( $new_instance, $old_instance ) {
		$instance = wp_parse_args( $old_instance, $this->get_defaults() );
		$instance['title'] = empty( $new_instance['title'] ) ? null : esc_attr( $new_instance['title'] );
		$instance['no_child_behavior'] = empty( $new_instance['no_child_behavior'] ) ? '' : $new_instance['no_child_behavior'];
		$instance['levels'] = is_numeric( $new_instance['levels'] ) ? $new_instance['levels'] : -1;
		
		return $instance;
	}
	
	function widget( $args, $instance ) {
		global $post;
		if ( ! is_object( $post ) || 'page' !== $post->post_type ) {
			/*print ( "\n<!-- This is either not a page, or the post object is not set -->\n" );*/
			return;
		}
		
		extract( $args );
		$no_children = false;
		$instance = wp_parse_args( $instance, $this->get_defaults() );
		
		$top = $post->ID;
		$classes = array( 'current-menu-item', 'current_page_item' );
		$args = array( 'depth' => $instance['levels'], 'title_li' => null, 'child_of' => $top, 'echo' => 0 );
		/*print( "\n<!-- This is a top-level page, and we are getting ready to list pages with the following args:\n" );
		var_dump( $args );
		print( "\n-->\n" );*/
		$children = wp_list_pages( $args );
		
		if ( empty( $children ) ) {
			$classes = array( 'current-menu-parent', 'has-submenu', 'current-page-parent', 'current_page_parent', 'current_page_ancestor' );
			$ancestors = get_ancestors( $post->ID, 'page' );
			/*print( "\n<!-- The ancestors element looks like:\n" );
			var_dump( $ancestors );
			print( "\n-->\n" );*/
			$continue = false;
			if ( empty( $ancestors ) ) {
				if ( empty( $instance['no_child_behavior'] ) )
					return false;
				if ( 'current' === $instance['no_child_behavior'] )
					$continue = true;
			}
			if ( ! $continue ) {
				$top = array_shift( $ancestors );
				$args = array( 'depth' => $instance['levels'], 'title_li' => null, 'child_of' => $top, 'echo' => 0 );
				/*print( "\n<!-- Getting ready to use the following args:\n" );
				var_dump( $args );
				print( "\n-->\n" );*/
				$children = wp_list_pages( $args );
			}
		}
		
		array_push( $classes, 'menu-item', 'page-item-' . $top, 'page_item' );
		
		
		if ( empty( $children ) && 'top' === $instance['no_child_behavior'] ) {
			$no_children = true;
			$children = wp_list_pages( array( 'depth' => 1, 'title_li' => null, 'echo' => 0, 'include' => $top ) );
		}
		echo $before_widget;
		
		if ( ! empty( $instance['title'] ) )
			echo $before_title . esc_attr( $instance['title'] ) . $after_title;
		
		echo '<nav><ul class="menu">';
		if ( 'top' !== $instance['no_child_behavior'] && ! $no_children ) {
			echo '<li class="' . implode( ' ', $classes ) . '" id="menu-item-' . $top . '"><a href="' . get_permalink( $top ) . '" title="' . apply_filters( 'the_title_attribute', get_the_title( $top ) ) . '">' . get_the_title( $top ) . '</a>';
			echo '<ul class="children">';
		}
		echo $children;
		if ( 'top' !== $instance['no_child_behavior'] && ! $no_children ) {
			echo '</ul></li>';
		}
		echo '</ul></nav>';
		
		echo $after_widget;
	}
}
?>