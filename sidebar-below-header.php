<?php global $ten321; ?>
<?php if ( ! array_key_exists( 'below-header', $ten321->get_default_sidebars() ) ) { return; } ?>
<?php
if( is_active_sidebar('below-header') ) {
?>
<aside class="sidebar below-header" role="complementary">
	<ul class="widget-area">
    	<?php dynamic_sidebar('below-header') ?>
    </ul>
</aside>
<?php
}
?>