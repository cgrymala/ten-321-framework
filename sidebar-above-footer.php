<?php global $ten321; ?>
<?php if ( ! array_key_exists( 'above-footer', $ten321->get_default_sidebars() ) ) { return; } ?>
<?php
if( is_active_sidebar('above-footer') ) {
?>
<aside class="sidebar above-footer" role="complementary">
	<ul class="widget-area">
    	<?php dynamic_sidebar('above-footer') ?>
    </ul>
</aside>
<?php
}
?>