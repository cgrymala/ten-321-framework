<?php global $ten321; ?>
<?php
/**
 * Outputs the secondary navigation custom menu, if that menu has been defined
 * @uses apply_filters() to apply the ten321-nav-args filter to the args array
 * 		for the wp_nav_menu() function. That filter sends the array as the first
 * 		param and the string 'secondary' as the second parameter
 */
if( function_exists( 'wp_nav_menu' ) && apply_filters( 'ten321_has_nav_menu', has_nav_menu( 'secondary' ), 'secondary' ) ) {
	wp_nav_menu( apply_filters( 'ten321-nav-args', array(
		'theme_location' => 'secondary',
        'container' => 'nav',
		'container_class' => 'secondary-nav',
        'menu_id' => 'secondary-nav',
		'fallback_cb' => array( $ten321, 'ten321_secondarynav_fallback' ),
	), 'secondary' ) );
}
?>