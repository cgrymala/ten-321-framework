<?php global $ten321; ?>
<?php if ( ! array_key_exists( 'secondary', $ten321->get_default_sidebars() ) ) { return; } ?>
<?php if( apply_filters( 'ten321_is_active_sidebar', is_active_sidebar('secondary'), 'secondary' ) ) { ?>

<aside class="sidebar secondary-sidebar column" role="complementary">
	<ul class="widget-area">
    	<?php dynamic_sidebar('secondary') ?>
    </ul>
</aside>

<?php } ?>