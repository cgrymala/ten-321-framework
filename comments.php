<?php
/**
 * The comments template
 */
global $ten321;
if ( post_password_required() || ( ! comments_open() && 0 == get_comments_number() ) || ! post_type_supports( get_post_type(), 'comments' ) )
	return;
?>
<section id="comments" class="comments-area">
	<h2 class="comments-title">
		<?php
            printf( _n( 'One response to &ldquo;%2$s&rdquo;', '%1$s responses to &ldquo;%2$s&rdquo;', get_comments_number(), $ten321->text_domain ),
                number_format_i18n( get_comments_number() ), '<span>' . get_the_title() . '</span>' );
        ?>
    </h2>
    
		<ol class="commentlist">
			<?php wp_list_comments( array( 'callback' => array( $ten321, 'comment_list' ) ) ) ?>
		</ol>

	<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
    <nav id="comment-nav-below" class="navigation" role="navigation">
        <h1 class="assistive-text section-heading"><?php _e( 'Comment navigation', $ten321->text_domain ); ?></h1>
        <div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', $ten321->text_domain ) ); ?></div>
        <div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', $ten321->text_domain ) ); ?></div>
    </nav>
    <?php endif; // check for comment navigation ?>
    
	<?php comment_form(); ?>
</section>