<?php global $ten321; ?>
<?php
/**
 * Outputs the primary navigation custom menu, if that menu has been defined
 * @uses apply_filters() to apply the ten321-nav-args filter to the args array
 * 		for the wp_nav_menu() function. That filter sends the array as the first
 * 		param and the strings 'primary' as the second parameter
 */
if( function_exists( 'wp_nav_menu' ) && apply_filters( 'ten321_has_nav_menu', has_nav_menu( 'primary' ), 'primary' ) ) {
	wp_nav_menu( apply_filters( 'ten321-nav-args', array(
		'theme_location' => 'primary',
        'container' => 'nav',
		'container_class' => 'primary-nav',
        'menu_id' => 'primary-nav',
		'fallback_cb' => array( $ten321, 'ten321_primarynav_fallback' ),
	), 'primary' ) );
}
?>