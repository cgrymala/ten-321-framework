<?php global $ten321; ?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
<title><?php 
$page_title = wp_title( '|', false, 'right' ); 
$page_title .= get_bloginfo( 'name' );
$site_description = get_bloginfo( 'description', 'display' );
if ( $site_description && ( is_home() || is_front_page() ) )
	$page_title .= " | $site_description";
$paged = get_query_var( 'paged' );
$page = get_query_var( 'page' );
if ( $paged >= 2 || $page >= 2 )
	$page_title .= ' | ' . sprintf( __( 'Page %s', $ten321->text_domain ), max( $paged, $page ) );

echo apply_filters( 'ten321-page-title-element', $page_title );
?></title>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<?php
get_template_part('analytics');
?>
<?php if( file_exists( get_stylesheet_directory() . '/favicon.ico' ) ) { ?>
<link rel="shortcut icon" href="<?php bloginfo( 'stylesheet_directory' ) ?>/favicon.ico" type="image/x-icon" /> 
<link rel="icon" href="<?php bloginfo( 'stylesheet_directory' ) ?>/favicon.ico" type="image/x-icon" /> 
<?php } else { ?>
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" /> 
<link rel="icon" href="/favicon.ico" type="image/x-icon" /> 
<?php } ?>
<?php if( file_exists( get_stylesheet_directory() . '/apple-touch-icon.png' ) ) { ?>
<link rel="apple-touch-icon" href="<?php bloginfo( 'stylesheet_directory' ) ?>/apple-touch-icon.png" />
<?php } else { ?>
<link rel="apple-touch-icon" href="/apple-touch-icon.png" />
<?php } ?>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php wp_head() ?>
</head>