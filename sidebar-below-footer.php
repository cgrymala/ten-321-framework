<?php global $ten321; ?>
<?php if ( ! array_key_exists( 'below-footer', $ten321->get_default_sidebars() ) ) { return; } ?>
<?php
if( is_active_sidebar('below-footer') ) {
?>
<aside class="sidebar below-footer" role="complementary">
	<ul class="widget-area">
    	<?php dynamic_sidebar('below-footer') ?>
    </ul>
</aside>
<?php
}
?>