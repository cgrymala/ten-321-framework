<?php global $ten321; ?>
<?php
/**
 * Outputs the sidebar and side-second navigation custom menus, if those menus have been defined
 * @uses apply_filters() to apply the ten321-nav-args filter to the args array
 * 		for the wp_nav_menu() function. That filter sends the array as the first
 * 		param and the strings 'sidebar' and 'side-second' as the second parameters, 
 * 		respectively
 */
if( function_exists( 'wp_nav_menu' ) && apply_filters( 'ten321_has_nav_menu', has_nav_menu( 'sidebar' ), 'sidebar' ) ) {
	wp_nav_menu( apply_filters( 'ten321-nav-args', array(
		'theme_location' => 'sidebar',
        'container' => 'nav',
		'container_class' => 'sidebar-nav',
        'menu_id' => 'sidebar-nav',
		'fallback_cb' => array( $ten321, 'ten321_sidebarnav_fallback' ),
	), 'sidebar' ) );
}
if( function_exists( 'wp_nav_menu' ) && apply_filters( 'ten321_has_nav_menu', has_nav_menu( 'side-second' ), 'side-second' ) ) {
	wp_nav_menu( apply_filters( 'ten321-nav-args', array(
		'theme_location' => 'side-second-nav',
        'container' => 'nav',
		'container_class' => 'side-second-nav',
        'menu_id' => 'side-second-nav',
		'fallback_cb' => array( $ten321, 'ten321_side_secondnav_fallback' ),
	), 'side-second' ) );
}
?>