<?php global $ten321; ?>
<?php get_template_part( 'head', $post->post_name ) ?>
<body <?php body_class( array( 'default', 'no-js' ) ); ?>>
<?php do_action( 'ten-321-after-body-tag' ) ?>
<?php get_sidebar( 'above-header' ) ?>
<?php get_template_part( 'nav', 'header' ) ?>
<?php do_action( 'ten-321-before-header' ) ?>
<header role="banner">
	<hgroup>
    	<?php if( is_front_page() ) { ?>
		<h1><?php bloginfo( 'name' ) ?></h1>
        <?php } else { ?>
        <a href="<?php bloginfo( 'url' ) ?>"><h1><?php bloginfo( 'name' ) ?></h1></a>
        <?php } ?>
        <h2><?php bloginfo( 'description' ) ?></h2>
        <?php do_action( 'ten-321-after-description' ) ?>
    </hgroup>
</header>
<?php do_action( 'ten-321-after-header' ) ?>
<?php get_sidebar( 'below-header' ) ?>
<?php get_template_part( 'nav', 'primary' ) ?>
<?php get_template_part( 'nav', 'secondary' ) ?>
