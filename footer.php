<?php global $ten321; ?>
<?php get_template_part( 'nav', 'footer' ) ?>
<?php get_sidebar( 'above-footer' ) ?>
<?php do_action( 'ten-321-before-footer' ) ?>
<footer>
<?php
if( empty( $ten321->options['footer-text'] ) ) {
	$ten321->options['footer-text'] = '
    <details>
    	<summary>&copy; ' . ( ( 2011 != date("Y") ? '2011-' : '' ) ) . date("Y") . '</summary>
        <p>This theme is based on the Ten-321 Framework, which was developed by <a href="http://ten-321.com/">Ten-321 Enterprises</a>.</p>
    </details>';
}
echo apply_filters( 'ten321-footer-text', $ten321->options['footer-text'] );
?>
</footer>
<?php do_action( 'ten-321-after-footer' ) ?>
<?php get_sidebar( 'below-footer' ) ?>
<?php wp_footer() ?>
<?php do_action( 'ten-321-end' ) ?>
</body>
</html>