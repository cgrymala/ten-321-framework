<?php global $ten321; ?>
<?php
if( !class_exists( 'ten321_framework' ) ) {
	/**
	 * Provide methods and variables to build the WordPress theme
	 * @package ten321-framework
	 * @version 1
	 * @todo Add ability to turn admin menus on and off
	 */
	class ten321_framework {
		var $options = array();
		var $settings_fields_notes = array();
		var $text_domain = 'ten321-framework';
		/**
		 * Construct our ten321_framework object
		 * @uses ten321_framework::_setup()
		 * @uses ten321_framework::_get_options()
		 */
		function __construct() {
			$this->_setup();
			$this->_get_options();
			
			if ( ! is_admin() ) {
				add_action( 'template_redirect', array( $this, 'front_page_query' ) );
				add_action( 'ten321-before-content', array( $this, 'before_content' ) );
				add_action( 'ten321-after-content', array( $this, 'after_content' ) );
				add_action( 'ten321-after-loop', array( $this, 'ten321_pagination' ) );
			}
		}
		
		/**
		 * Perform various set up tasks
		 * @uses ten321_framework::add_thumbnail_support()
		 * @uses ten321_framework::add_format_support()
		 * @uses ten321_framework::add_feedlink_support()
		 * @uses ten321_framework::add_header_support()
		 * @uses ten321_framework::add_background_support()
		 * @uses load_theme_textdomain()
		 * @uses ten321_framework::register_sidebars()
		 * @uses ten321_framework::register_nav_menus()
		 * @uses ten321_framework::front_end_style_scripts()
		 * @uses wp_register_style()
		 * @uses add_action() to add ten321_framework::get_menu_list(), ten321_framework::add_submenu_page() and ten321_framework::remvoe_admin_menus() to the admin_menu action
		 * @uses wp_enqueue_style()
		 * @uses add_filter() to add ten321_framework::body_class() to the body_class filter
		 */
		function _setup() {
			$this->add_thumbnail_support();
			$this->add_format_support();
			$this->add_feedlink_support();
			$this->add_header_support();
			$this->add_background_support();
			
			load_theme_textdomain( 'ten321' );
			
			$this->register_sidebars();
			$this->register_nav_menus();
			
			if( !is_admin() )
				add_action( 'wp_enqueue_scripts', array( $this, 'front_end_style_scripts' ) );
			if( is_admin() ) {
				add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ) );
			}
			
			add_filter( 'body_class', array( $this, 'body_class' ), 10, 2 );
			
			add_action( 'widgets_init', array( $this, 'register_widgets' ) );
			
			add_filter( 'sanitize_file_name_chars', array( $this, 'sanitize_file_name_chars' ) );
		}
		
		/**
		 * Retrieve our theme options
		 * @uses ten321_framework::$options
		 * @uses apply_filters() to apply any filters that have been added to ten321-option-defaults
		 * @uses get_option() to retrieve the list of ten321-theme-options from the database
		 * @return array the filtered list of options
		 */
		function _get_options( $option = null, $default = false ) {
			if ( empty( $this->options ) )
				$this->options = array_merge( 
					apply_filters( 'ten321-option-defaults', array( 
						'footer-text' => null, 
						'nav_menus' => array(), 
						'sidebars' => array(), 
						'admin_menus' => array(), 
						
						'show-author' => 1, 
						'show-bio' => 1, 
						'show-date' => 1, 
						'show-tags' => 1, 
						'show-cats' => 1, 
						'exclude-cats' => null,  
					) 
				), get_option( 'ten321-theme-options', array() ) );
			
			if ( ! is_null( $option ) )
				return ( array_key_exists( $option, $this->options ) ) ? $this->options[$option] : $default;
			
			return $this->options;
		}
		
		/**
		 * Retrieve the list of menus in the admin dashboard 
		 * Sets the $admin_menus property for this class, based on the global $menu var
		 * @uses $GLOBALS['menu']
		 * @uses ten321_framework::$admin_menus
		 * @return void
		 */
		function get_menu_list() {
			$tmp_menus = $GLOBALS['menu'];
			$this->admin_menus = array();
			foreach( $tmp_menus as $k=>$m ) {
				if( !empty( $m[0] ) && !empty( $m[2] ) )
					$this->admin_menus[$m[2]] = $m[0];
			}
		}
		
		/**
		 * Add support for featured images
		 * @uses add_theme_support()
		 * @uses apply_filters() to apply the ten321-post-thumbnails filter to the array of 
		 * 		post types that the thumbnail support should be added to
		 */
		function add_thumbnail_support() {
			$post_types = apply_filters( 'ten321-post-thumbnails', array() );
			if( !empty( $post_types ) )
				add_theme_support( 'post-thumbnails', $post_types );
			else
				add_theme_support( 'post-thumbnails' );
		}
		
		/**
		 * Add support for custom post formats
		 * @uses add_theme_support()
		 * @uses apply_filters() to apply the ten321-post-formats filter to the array of 
		 * 		custom post formats to be registered
		 */
		function add_format_support() {
			add_theme_support( 'post-formats', apply_filters( 'ten321-post-formats', array( 'aside', 'gallery', 'article', 'video' ) ) );
		}
		
		/**
		 * Add support for automatic feed links in the head
		 * @uses add_theme_support()
		 */
		function add_feedlink_support() {
			add_theme_support( 'automatic-feed-links' );
		}
		
		/**
		 * Add support for a custom image header
		 * @uses apply_filters() to apply the ten321-header_textcolor filter to the hex color 
		 * 		that should be used for the header text
		 * @uses apply_filters() to apply the ten321-header_image filter to the location of the 
		 * 		default header image
		 * @uses apply_filters() to apply the ten321-header_image_width filter to the int that 
		 *		should be used for the image width
		 * @uses apply_filters() to apply the ten321-header_image_height filter to the int that 
		 * 		should be used for the image height
		 * @uses apply_filters() to apply the ten321-no_header_text filter to the boolean that 
		 * 		indicates whether the header should include text (use the ten321-header_textcolor 
		 * 		filter to change that to an empty value if you set this to false).
		 * @uses apply_filters() to apply the ten321-admin-image-div-callback filter to assign a 
		 * 		callback function to output custom image div on admin header management page
		 */
		function add_header_support() {
			if( !defined( 'HEADER_TEXTCOLOR' ) )
				define('HEADER_TEXTCOLOR', apply_filters( 'ten321-header_textcolor', 'ffffff' ) );
			if( !defined( 'HEADER_IMAGE' ) )
				define('HEADER_IMAGE', apply_filters( 'ten321-header_image', trailingslashit( get_stylesheet_directory_uri() ).'images/banner.jpg' ) );
			if( !defined( 'HEADER_IMAGE_WIDTH' ) )
				define('HEADER_IMAGE_WIDTH', apply_filters( 'ten321-header_image_width', 960 ) );
			if( !defined( 'HEADER_IMAGE_HEIGHT' ) )
				define('HEADER_IMAGE_HEIGHT', apply_filters( 'ten321-header_image_height', 200 ) );
			if( !defined( 'NO_HEADER_TEXT' ) )
				define('NO_HEADER_TEXT', apply_filters( 'ten321-no_header_text', true ) );
			
			$header_args = apply_filters( 'ten321-header-args', array(
				'width'              => HEADER_IMAGE_WIDTH, 
				'height'             => HEADER_IMAGE_HEIGHT, 
				'header-text'        => NO_HEADER_TEXT ? false : true, 
				'default-text-color' => HEADER_TEXTCOLOR, 
				'default-image'      => HEADER_IMAGE, 
				'wp-head-callback'   => array( $this, 'header_callback' ), 
				'admin-head-callback'=> array( $this, 'admin_header_callback' ), 
				'admin-preview-callback' => apply_filters( 'ten321-admin-image-div-callback', '' ), 
			) );
			
			add_theme_support( 'custom-header', $header_args );
		}
		
		/**
		 * Outputs any extra styles for the header image
		 * @uses get_header_image()
		 * @uses NO_HEADER_TEXT
		 * @uses HEADER_IMAGE_HEIGHT
		 * @uses HEADER_IMAGE_WIDTH
		 * @uses apply_filters() to apply the ten321-header-callback filter to the output that
		 * 		is echoed
		 */
		function header_callback() {
			$output = '<style type="text/css">
header hgroup, 
header .header-group { 
	background: url(' . get_header_image() . ');
	overflow: hidden;
	width: ' . HEADER_IMAGE_WIDTH . 'px;
	height: ' . HEADER_IMAGE_HEIGHT . 'px;
}';
			if( defined('NO_HEADER_TEXT') && NO_HEADER_TEXT ) {
				$output .= '
header hgroup a, 
header .header-group a {
	display: block;
	width: ' . HEADER_IMAGE_WIDTH . 'px;
	height: ' . HEADER_IMAGE_HEIGHT . 'px;
}
header hgroup h1, 
header hgroup h2, 
header .header-group h1, 
header .header-group h2 {
	position: absolute;
	left: -9999em;
}';
			}
			$output .= '
</style>';
			echo apply_filters( 'ten321-header-callback', $output );
		}
		
		/**
		 * Outputs any extra styles for the header image
		 * @uses HEADER_IMAGE_HEIGHT
		 * @uses HEADER_IMAGE_WIDTH
		 * @uses apply_filters() to apply the ten321-admin-header-callback filter to the output that
		 * 		is echoed
		 */
		function admin_header_callback() {
			$output = '<style type="text/css">
	#headimg {
		width: ' . HEADER_IMAGE_WIDTH . 'px;
		height: ' . HEADER_IMAGE_HEIGHT . 'px;
	}
</style>';
			echo apply_filters( 'ten321-admin-header-callback', $output );
		}
		
		/**
		 * Adds support for custom background images
		 * @todo
		 */
		function add_background_support() {
		}
		
		/**
		 * Retrieve the default list of sidebars to use in this theme
		 * @uses apply_filters() to apply the ten321-sidebars filter to the array
		 */
		function get_default_sidebars() {
			return apply_filters( 'ten321-sidebars', array(
				'primary'		=> array( 
					'name'			=> __( 'Primary Sidebar', $this->text_domain ), 
					'id'			=> 'primary', 
					'description'	=> __( 'An optional widgetized area that appears in the left column of the body area on the site.', $this->text_domain ),
				),
				'secondary'		=> array( 
					'name'			=> __( 'Secondary Sidebar', $this->text_domain ), 
					'id'			=> 'secondary', 
					'description'	=> __( 'An optional widgetized area that appears in the right column of the body area on the site.', $this->text_domain ),
				),
				'above-header'	=> array( 
					'name'			=> __( 'Above Header', $this->text_domain ), 
					'id'			=> 'above-header', 
					'description'	=> __( 'An optional widgetized area that appears above the header (and above the header navigation, if enabled) by default.', $this->text_domain ),
				),
				'below-header'	=> array( 
					'name'			=> __( 'Below Header', $this->text_domain ), 
					'id'			=> 'below-header', 
					'description'	=> __( 'An optional widgetized area that appears between the header and the primary navigation menu by default.', $this->text_domain ),
				),
				'above-footer'	=> array( 
					'name'			=> __( 'Above Footer', $this->text_domain ), 
					'id'			=> 'above-footer', 
					'description'	=> __( 'An optional widgetized area that appears between the footer navigation menu and the footer itself, by default.', $this->text_domain ),
				),
				'below-footer'	=> array( 
					'name'	=> __( 'Below Footer', $this->text_domain ), 
					'id'			=> 'below-footer', 
					'description'	=> __( 'An optional widgetized area that appears at the very bottom of the page (below the footer)', $this->text_domain ),
				),
			) );
		}
		
		/**
		 * Get a list of the custom navigation menus that should be registered
		 * @uses apply_filters() to apply the ten321-nav-menus filter to the array of menus
		 * @return array an associative array of menu ID and menu title
		 */
		function get_default_menus() {
			return apply_filters( 'ten321-nav-menus', array(
				'header-nav'	=> 'Header Navigation',
				'footer-nav'	=> 'Footer Navigation',
				'primary'		=> 'Primary Navigation',
				'secondary'		=> 'Secondary Navigation',
				'sidebar'		=> 'Sidebar Navigation',
				'side-second'	=> 'Secondary Sidebar Navigation',
			) );
		}
		
		/**
		 * Adds support for widgetized areas
		 * @uses register_sidebars()
		 * @uses apply_filters() to apply the ten321-sidebar-numbers filter to the number of 
		 *		widget areas that need to be registered
		 * @uses apply_filters() to apply the ten321-sidebar-defauls to an empty array that can 
		 * 		be used to change the default values used for the widget areas
		 */
		function register_sidebars() {
			$sidebars = $this->get_default_sidebars();
			$this->_get_options();
			
			if( empty( $this->options['sidebars'] ) ) {
				$sb = $sidebars;
			} else {
				$sb = array();
				foreach( $sidebars as $k=>$s ) {
					if( in_array( $k, $this->options['sidebars'] ) )
						$sb[$k] = $s;
				}
			}
			$sidebar_defaults = apply_filters( 'ten321-sidebar-defaults', array(
				'name'			=> __( 'Sidebar %d' ),
				'id'			=> 'sidebar-%d',
				'description'	=> '',
				'before_widget'	=> '<li id="%1$s" class="widget %2$s">',
				'after_widget'	=> '</li>',
				'before_title'	=> '<h2 class="widgettitle">',
				'after_title'	=> '</h2>',
			) );
			$i = 1;
			foreach( $sb as $sidebar ) {
				$s = $sidebar_defaults;
				$s['name'] = sprintf( $s['name'], $i );
				$s['id'] = sprintf( $s['id'], $i );
				$sidebar = array_merge( $s, $sidebar );
				register_sidebar( $sidebar );
				$i++;
			}
			return;
			
			/**
			 * Alternative method of registering sidebars follows.
			 * @deprecated
			 */
			$sidebar_names = apply_filters( 'ten321-sidebar-names', array(
				1	=> __( 'Primary Sidebar', $this->text_domain ),
				2	=> __( 'Secondary Sidebar', $this->text_domain ),
				3	=> __( 'Above Header', $this->text_domain ),
				4	=> __( 'Below Header', $this->text_domain ),
				5	=> __( 'Above Footer', $this->text_domain ),
				6	=> __( 'Below Footer', $this->text_domain ),
			) );
			register_sidebars( count( $sidebar_names ), apply_filters( 'ten321-sidebar-defaults', array() ) );
			global $wp_registered_sidebars;
			foreach( $sidebar_names as $k=>$v ) {
				$wp_registered_sidebars['sidebar-' . $k]['name'] = $v;
			}
			return;
		}
		
		/**
		 * Adds support for custom menus
		 * @uses register_nav_menus()
		 * @uses apply_filters() to apply the ten321-nav-menus filter to the array of menus
		 * 		that should be created
		 */
		function register_nav_menus() {
			$nav_menus = $this->get_default_menus();
			$this->_get_options();
			
			if( empty( $this->options['nav_menus'] ) )
				return register_nav_menus( $nav_menus );
			
			$nm = array();
			foreach( $nav_menus as $k=>$v ) {
				if( in_array( $k, $this->options['nav_menus'] ) )
					$nm[$k] = $v;
			}
			return register_nav_menus( $nm );
		}
		
		/**
		 * Register any widgets that are included in the theme
		 */
		function register_widgets() {
			/**
			 * Register the page list widget
			 */
			if ( ! class_exists( 'ten321_navigation_menu' ) )
				require_once( WP_CONTENT_DIR . '/themes/ten-321-framework/' . 'widgets/navigation.php' );
			register_widget( 'ten321_navigation_menu' );
		}
		
		/**
		 * Register and enqueue all necessary scripts & styles for admin area
		 * @uses wp_register_script()
		 * @uses wp_register_style()
		 * @uses wp_enqueue_script()
		 * @uses wp_enqueue_style()
		 * @uses do_action() to call the ten321-enqueue-admin-styles and ten321-enqueue-admin-scripts actions
		 */
		function admin_enqueue_scripts() {
			if ( ! is_admin() )
				return;
			
			wp_register_style( 'ten321-admin-styles', get_bloginfo( 'template_directory' ) . '/admin/style-admin.css', array(), 0.1, 'all' );
			add_action( 'admin_menu', array( $this, 'get_menu_list' ), 50 );
			add_action( 'admin_menu', array( $this, 'add_submenu_page' ), 51 );
			add_action( 'admin_menu', array( $this, 'remove_admin_menus' ), 52 );
			if( isset( $_REQUEST['page'] ) && 'ten321-options' == $_REQUEST['page'] ) {
				wp_enqueue_style( 'ten321-admin-styles' );
			}
			do_action( 'ten321-enqueue-admin-styles' );
			do_action( 'ten321-enqueue-admin-scripts' );
		}
		
		/**
		 * Register and enqueue all of the necessary scripts and styles
		 * @uses wp_register_script();
		 * @uses wp_register_style()
		 * @uses wp_enqueue_script()
		 * @uses wp_enqueue_style()
		 * @uses do_action() to call the ten321-enqueue-styles and ten321-enqueue-scripts actions
		 */
		function front_end_style_scripts() {
			global $wp_scripts;
			$http = $this->is_ssl() ? 'https:' : 'http:';
			wp_register_script( 'html5-shiv', $http . '//html5shim.googlecode.com/svn/trunk/html5.js', array(), '1', false );
			if ( ! $this->is_ssl() ) {
				wp_register_style( 'yui-reset', $http . '//yui.yahooapis.com/2.9.0/build/reset/reset-min.css', array(), '2.9.0', 'all' );
				wp_register_style( 'ten321', get_bloginfo( 'stylesheet_url' ), array( 'yui-reset' ), apply_filters( 'ten321-stylesheet-version', '0.1' ), 'all' );
			} else {
				wp_register_style( 'yui-reset', str_replace( 'http:', 'https:', get_bloginfo( 'template_directory' ) ) . '/yui-reset.secure.css', array(), '2.9.0', 'all' );
				wp_register_style( 'ten321', str_replace( 'http:', 'https:', get_bloginfo( 'stylesheet_url' ) ), array( 'yui-reset' ), apply_filters( 'ten321-stylesheet-version', '0.1' ), 'all' );
			}
			if( is_object( $wp_scripts ) )
				$wp_scripts->add_data( 'html5-shiv', 'conditional', 'lt IE 9' );
			wp_enqueue_script( 'html5-shiv' );
			wp_enqueue_style( 'yui-reset' );
			wp_enqueue_style( 'ten321' );
			do_action( 'ten321-enqueue-styles' );
			do_action( 'ten321-enqueue-scripts' );
		}
		
		/**
		 * Checks to see if we are running through SSL or not
		 */
		function is_ssl() {
			$is_ssl = array_key_exists( 'HTTPS', $_SERVER ) && 'on' == $_SERVER['HTTPS'];
			return apply_filters( 'ten-321-is-ssl', $is_ssl );
		}
		
		/**
		 * Alters the body class array
		 * Adds information indicating which nav menus and which widgetized areas are active, 
		 * 		the slug of the top-level ancestor of the current page to the list of CSS classes 
		 * 		applied to the body element
		 * @return array the array of classes to apply to the body
		 *
		 * @uses ten321_framework::get_default_sidebars()
		 * @uses apply_filters() to apply the ten321_is_active_sidebar filter to the results of the is_active_sidebar() function
		 * @uses is_active_sidebar()
		 * @uses apply_filters() to apply the ten321_has_nav_menu filter to the has_nav_menu() function results
		 * @uses has_nav_menu()
		 * @uses ten321_framework::get_top_level() to retrieve the post_name of the top-level ancestor
		 * @uses apply_filters() to apply the ten321-body_class filter to the final array
		 */
		function body_class( $classes, $class='' ) {
			$sc = array_keys( $this->get_default_sidebars() );
			foreach( $sc as $s ) {
				if( apply_filters( 'ten321_is_active_sidebar', is_active_sidebar( $s ), $s ) )
					$classes[] = 'has-sidebar-' . $s;
			}
			
			$sc = $this->get_default_menus();
			foreach( $sc as $k=>$s ) {
				if( apply_filters( 'ten321_has_nav_menu', has_nav_menu( $k ), $k ) )
					$classes[] = 'has-nav-' . $k;
			}
			
			if ( $tmp = $this->get_top_level() ) {
				$classes[] = $tmp->post_name;
			}
			
			if ( ! empty( $class ) ) {
				if ( !is_array( $class ) )
					$class = preg_split( '#\s+#', $class );
				$classes = array_merge( $classes, $class );
			} else {
				// Ensure that we always coerce class to being an array.
				$class = array();
			}
			
			$classes = array_unique( $classes );
			
			return apply_filters( 'ten321-body_class', $classes, $class );
		}
		
		/**
		 * Applies the WordPress template hierarchy to the loop portion of the template
		 * @param string $loopname the slug of the loop template to attempt to retrieve first
		 * @uses locate_template()
		 * @uses apply_filters() to apply the ten321-loop-templates filter to the array of 
		 * 		template names that should be searched
		 */
		function get_loop( $loopname=null ) {
			global $post, $wp_query;
			
			$templates = array();
			if( $loopname )
				$templates[] = "loop-{$loopname}.php";
			
			if( is_home() )
				$templates[] = 'loop-home.php';
			if( is_front_page() )
				$templates[] = 'loop-front-page.php';
			if( is_404() )
				$templates[] = 'loop-404.php';
			if( is_search() )
				$templates[] = 'loop-search.php';
			if( is_date() )
				array_push( $templates, 'loop-date.php', 'loop-archive.php' );
			if( is_author() ) {
				$u = get_queried_object();
				if ( is_object( $u ) ) {
					if( isset( $u->user_nicename ) )
						$templates[] = "loop-author-{$u->user_nicename}.php";
					if( isset( $u->ID ) )
						$templates[] = "loop-author-{$u->ID}.php";
					array_push( $templates, 'loop-author.php', 'loop-archive.php' );
				}
			}
			if( is_category() ) {
				$c = get_queried_object();
				if ( is_object( $c ) ) {
					if( isset( $c->slug ) )
						$templates[] = "loop-category-{$c->slug}.php";
					if( isset( $c->term_id ) )
						$templates[] = "loop-category-{$c->term_id}.php";
					array_push( $templates, 'loop-category.php', 'loop-archive.php' );
				}
			}
			if( is_tag() ) {
				$t = get_queried_object();
				if ( is_object( $t ) ) {
					if( isset( $t->slug ) )
						$templates[] = "loop-tag-{$t->slug}.php";
					if( isset( $t->term_id ) )
						$templates[] = "loop-tag-{$t->term_id}.php";
					array_push( $templates, 'loop-tag.php', 'loop-archive.php' );
				}
			}
			if( is_tax() ) {
				$term = get_queried_object();
				if ( is_object( $term ) ) {
					$taxonomy = $term->taxonomy;
					array_push( $templates, "loop-taxonomy-$taxonomy-{$term->slug}.php", "loop-taxonomy-$taxonomy.php", 'loop-taxonomy.php', 'loop-archive.php' );
				}
			}
			if( is_archive() ) {
				$post_type = get_query_var( 'post_type' );
				array_push( $templates, "loop-archive-{$post_type}.php", "loop-archive.php" );
			}
			if( is_single() ) {
				$post_type = get_query_var( 'post_type' );
				array_push( $templates, "loop-single-{$post_type}.php", "loop-single.php" );
			}
			if( is_attachment() ) {
				global $posts;
				$type = explode('/', $posts[0]->post_mime_type);
				array_push( $templates, "loop-{$type[0]}.php", "loop-{$type[1]}.php", "loop-{$type[0]}_{$type[1]}.php", "loop-single.php" );
			}
			if( is_page() ) {
				$id = get_queried_object_id();
				$template = get_post_meta($id, '_wp_page_template', true);
				$pagename = get_query_var('pagename');
				if ( !$pagename && $id > 0 ) {
					// If a static page is set as the front page, $pagename will not be set. Retrieve it from the queried object
					$post = get_queried_object();
					$pagename = $post->post_name;
				}
			
				if ( 'default' != $template )
					$templates[] = "loop-{$template}";
				if( $pagename )
					$templates[] = "loop-page-{$pagename}.php";
				if( $id )
					$templates[] = "loop-page-{$id}.php";
				
				$templates[] = 'loop-page.php';
			}
			
			$templates[] = 'loop.php';
			
			locate_template( apply_filters( 'ten321-loop-templates', $templates ), true, false );
		}
		
		/**
		 * Retrieve the top level page based on a provided page ID
		 * @param int $page_id the ID of the current page
		 * @return stdClass the top-level parent of the provided ID
		 */
		function get_top_level( $page_id=null ) {
			if ( ! isset( $GLOBALS['post'] ) )
				return false;
			
			if( is_null( $page_id ) )
				$page_id = $GLOBALS['post']->ID;
			$page = get_page( $page_id );
			while( !empty( $page->post_parent ) ) {
				$page = get_page( $page->post_parent );
			}
			return $page;
		}
		
		/**
		 * Function to act as the fallback for registered nav menus
		 * @param string $which the menu for which to generate a fallback
		 * @uses wp_list_pages() as the fallback for the primary nav menu
		 */
		function nav_fallback( $which=null ) {
			if( null === $which )
				return;
			
			if( 'primary' == $which ) {
				wp_list_pages();
			}
			return;
		}
		function ten321_headernav_fallback() {
			return $this->nav_fallback( 'header-nav' );
		}
		function ten321_primarynav_fallback() {
			return $this->nav_fallback( 'primary' );
		}
		function ten321_secondarynav_fallback() {
			return $this->nav_fallback( 'secondary' );
		}
		function ten321_sidebarnav_fallback() {
			return $this->nav_fallback( 'sidebar' );
		}
		function ten321_sidesecondnav_fallback() {
			return $this->nav_fallback( 'side-second' );
		}
		function ten321_footernav_fallback() {
			return $this->nav_fallback( 'footer-nav' );
		}
		
		/**
		 * Add the submenu page for theme options
		 * @uses add_theme_page() to add an options page under the Appearance menu
		 * @uses apply_filters() to apply the ten321_theme_options_page_title filter to the title of the page
		 * @uses apply_filters() to apply the ten321_theme_options_menu_title filter to the title of the menu item
		 * @uses ten321_framework::admin_page() as the callback to display the page
		 * @uses register_setting to whitelist the theme options
		 * @uses ten321_framework::sanitize_options() as the callback to sanitize the options
		 * @uses add_settings_section() to register the section of options
		 * @uses ten321_framework::settings_section() as the callback to output the settings section content
		 * @uses ten321_framework::add_settings_fields()
		 * @uses ten321_framework::$options
		 * @uses remove_menu_page() to remove any admin menus that should be hidden
		 */
		function add_submenu_page() {
			add_theme_page(
				apply_filters( 'ten321_theme_options_page_title', __( 'Ten-321 Theme Options', $this->text_domain ) ),
				apply_filters( 'ten321_theme_options_menu_title', __( 'Theme Options', $this->text_domain ) ),
				'manage_options',
				'ten321-options',
				array( $this, 'admin_page' )
			);
			register_setting( 'ten321-theme-options-group', 'ten321-theme-options', array( $this, 'sanitize_options' ) );
			add_settings_section( 'ten321-theme-options-group', __( 'Appearance Settings', $this->text_domain ), array( $this, 'settings_section' ), 'ten321-options' );
			add_settings_section( 'ten321-blog-options-group', __( 'Blog Options', $this->text_domain ), array( $this, 'settings_section' ), 'ten321-options' );
			$this->add_settings_fields();
			
			foreach( $this->options['admin_menus'] as $m ) {
				remove_menu_page( $m );
			}
		}
		
		/**
		 * Removes items that should be hidden from the admin dashboard menu
		 * I don't think this function is used right now
		 */
		function remove_admin_menus() {
			$hide_menus = apply_filters( 'ten321-hidden-menus', $this->options['admin_menus'] );
			global $menu;
			foreach( $menu as $k=>$m ) {
				if( array_key_exists( $k, $hide_menus ) )
					unset( $menu[$k] );
			}
		}
		
		/**
		 * Add the settings fields to our form
		 * @uses ten321_framework::_get_settings_fields() to retrieve the list of options form fields
		 * @uses add_settings_field()
		 */
		function add_settings_fields() {
			$fields = $this->_get_settings_fields();
			foreach( $fields as $field ) {
				$field = array_merge( array( 'id' => null, 'title' => null, 'type' => null, 'note' => null, 'multiple' => false, 'options' => null ), $field );
				if( !array_key_exists( 'label_for', $field ) )
					$field['label_for'] = $field['id'];
				
				add_settings_field( $field['id'], $field['title'], array( $this, 'do_settings_field' ), 'ten321-options', 'ten321-theme-options-group', $field );
			}
			
			$fields = $this->_get_blog_options_fields();
			foreach( $fields as $field ) {
				$field = array_merge( array( 'id' => null, 'title' => null, 'type' => null, 'note' => null, 'multiple' => false, 'options' => null ), $field );
				if( !array_key_exists( 'label_for', $field ) )
					$field['label_for'] = $field['id'];
				
				add_settings_field( $field['id'], $field['title'], array( $this, 'do_settings_field' ), 'ten321-options', 'ten321-blog-options-group', $field );
			}
		}
		
		/**
		 * Generate the list of fields/options for this theme
		 * @return array the list of options fields
		 * 
		 * @uses ten321_framework::get_default_sidebars()
		 * @uses apply_filters() to apply the ten321-settings-fields filter to the list of fields
		 */
		protected function _get_settings_fields() {
			$sidebars = $this->get_default_sidebars();
			$sideopts = array();
			foreach( $sidebars as $k=>$s ) {
				$side_opts[$k] = $s['name'];
			}
			$this->settings_fields_notes = apply_filters( 'ten321-settings-fields-notes', array(
				'footer-text'	=> sprintf( __( '<p>The HTML code that is used as the footer of your pages. Right now, the footer text is set to:</p><pre style="width: 55em; overflow-x: auto;"><code>%s</code></pre>', $this->text_domain ), esc_textarea( apply_filters( 'ten321-footer-text', $ten321->options['footer-text'] ) ) ),
				'sidebars'		=> __( '<p>Determine which widgetized areas should be available to users that are allowed to add widgets.</p>', $this->text_domain ),
				'nav_menus'		=> __( '<p>Determine which menu locations should be available to users that are allowed to manage custom navigation menus.</p>', $this->text_domain ),
				'admin_menus'	=> __( '<p>If you would like to hide some of the administation menus in order to keep the admin area from getting too cluttered, you can choose which menus to hide.</p>', $this->text_domain ),
			) );
			$fields = apply_filters( 'ten321-settings-fields', array(
				'footer-text' 	=> array( 'id' => 'footer-text', 'title' => __( 'Footer Text', $this->text_domain ), 'type' => 'textarea' ),
				'sidebars'		=> array( 'id' => 'sidebars', 'title' => __( 'Which widgetized areas should appear?', $this->text_domain ), 'type' => 'select', 'multiple' => true, 'options' => $side_opts ),
				'nav_menus'		=> array( 'id' => 'nav_menus', 'title' => __( 'Which custom menu areas should be active?', $this->text_domain ), 'type' => 'select', 'multiple' => true, 'options' => $this->get_default_menus() ),
				/* The options array really needs to be the key of the top array and the first item in the inner array */
				'admin_menus'	=> array( 'id' => 'admin_menus', 'title' => __( 'Which administration menus should be hidden in the admin dashboard?', $this->text_domain ), 'type' => 'select', 'multiple' => true, 'options' => $this->admin_menus ),
			) );
			foreach( $fields as $name=>$field ) {
				if( array_key_exists( $name, $this->settings_fields_notes ) )
					$fields[$name]['note'] = $this->settings_fields_notes[$name];
			}
			return $fields;
		}
		
		/**
		 * Generate the list of fields/options for the blog (front_page) page
		 * @return array the list of options fields
		 * 
		 * @uses apply_filters() to apply the ten321-blog-options-fields filter to the list of fields
		 */
		protected function _get_blog_options_fields() {
			$sidebars = $this->get_default_sidebars();
			$sideopts = array();
			foreach( $sidebars as $k=>$s ) {
				$side_opts[$k] = $s['name'];
			}
			
			$this->settings_fields_notes = apply_filters( 'ten321-blog-options-fields-notes', array(
			) );
			
			$cats = get_categories( array(
				'type'       => 'post', 
				'orderby'    => 'name', 
				'hide_empty' => 0, 
			) );
			
			$cat_opts = array();
			foreach( $cats as $c ) {
				$cat_opts[$c->term_id] = $c->name;
			}
			
			$fields = apply_filters( 'ten321-blog-options-fields', array(
				'show-author' 	=> array( 'id' => 'show-author', 'title' => __( 'Show author\'s name with each post?', $this->text_domain ), 'type' => 'select', 'options' => array( 1 => __( 'Yes', $this->text_domain ), 0 => __( 'No', $this->text_domain ) ) ),
				'show-bio'      => array( 'id' => 'show-bio', 'title' => __( 'Show the author\'s bio text with each post?', $this->text_domain ), 'type' => 'select', 'options' => array( 1 => __( 'Yes', $this->text_domain ), 0 => __( 'No', $this->text_domain ) ) ), 
				'show-date' 	=> array( 'id' => 'show-date', 'title' => __( 'Show the publish date with each post?', $this->text_domain ), 'type' => 'select', 'options' => array( 1 => __( 'Yes', $this->text_domain ), 0 => __( 'No', $this->text_domain ) ) ),
				'show-tags' 	=> array( 'id' => 'show-tags', 'title' => __( 'Show associated tags below each post?', $this->text_domain ), 'type' => 'select', 'options' => array( 1 => __( 'Yes', $this->text_domain ), 0 => __( 'No', $this->text_domain ) ) ),
				'show-cats' 	=> array( 'id' => 'show-cats', 'title' => __( 'Show associated categories below each post?', $this->text_domain ), 'type' => 'select', 'options' => array( 1 => __( 'Yes', $this->text_domain ), 0 => __( 'No', $this->text_domain ) ) ),
				'exclude-cats' 	=> array( 'id' => 'exclude-cats', 'title' => __( 'Exclude the following category(ies) from the blog archive:', $this->text_domain ), 'type' => 'select', 'multiple' => true, 'options' => $cat_opts ),
			) );
			foreach( $fields as $name=>$field ) {
				if( array_key_exists( $name, $this->settings_fields_notes ) )
					$fields[$name]['note'] = $this->settings_fields_notes[$name];
			}
			return $fields;
		}
		
		/**
		 * Output a specific option field
		 */
		function do_settings_field( $args ) {
			switch( $args['type'] ) {
				case 'textarea':
?>
		<textarea name="ten321-theme-options[<?php echo $args['label_for'] ?>]" id="<?php echo $args['label_for'] ?>" rows="8" cols="30" class="large-text"><?php echo esc_textarea( $this->options[$args['label_for']] ) ?></textarea>
<?php
				break;
				case 'select':
?>
		<select class="widefat<?php echo $args['multiple'] ? ' multiple' : '' ?>" name="ten321-theme-options[<?php echo $args['label_for'] ?>]<?php echo $args['multiple'] ? '[]" multiple="multiple' : '' ?>" id="<?php echo $args['label_for'] ?>">
<?php
					if( is_array( $args['options'] ) ) {
						foreach( $args['options'] as $value=>$label ) {
?>
			<option value="<?php echo $value ?>"<?php if( is_array( $this->options[$args['label_for']] ) ) { echo in_array( $value, $this->options[$args['label_for']] ) ? ' selected="selected"' : ''; } else { selected( $this->options[$args['label_for']], $value ); } ?>><?php echo $label ?></option>
<?php
						}
					}
?>
        </select>
<?php
				break;
				case 'checkbox':
					$args['multiple'] = true;
				case 'radio':
					foreach( $args['options'] as $value=>$label ) {
?>
		<input type="<?php echo $args['type'] ?>" value="<?php echo $value ?>" name="ten321-theme-options[<?php echo $args['label_for'] ?>]<?php echo $args['multiple'] ? '[]' : '' ?>" id="<?php echo $args['label_for'] ?>_<?php echo esc_attr( $value ) ?>"/> 
        <label for="<?php echo $args['label_for'] ?>_<?php echo esc_attr( $value ) ?>"><?php echo $label ?></label>
<?php
					}
				break;
				default:
?>
		<input class="widefat" type="<?php echo $args['type'] ?>" name="ten321-theme-options[<?php echo $args['label_for'] ?>]" id="<?php echo $args['label_for'] ?>" value="<?php echo $this->options[$args['label_for']] ?>"/>
<?php
			}
			if( !empty( $args['note'] ) ) {
?>
		<div class="note"><?php echo $args['note'] ?></div>
<?php
			}
		}
		
		/**
		 * Output any information to precede the settings section
		 */
		function settings_section() {
			return;
		}
		
		/**
		 * Display the admin options page
		 * @todo Add ability to import/export options. Import/export feature should handle theme options and widgets
		 */
		function admin_page() {
?>
<div class="wrap">
<?php
			if( isset( $_GET['settings-updated'] ) && 'true' === $_GET['settings-updated'] ) {
?>
<div class="updated fade"><p><?php _e( 'The settings were updated.' ) ?></p></div>
<?php
			}
?>
	<h2><?php echo apply_filters( 'ten321_theme_options_page_title', __( 'Ten-321 Theme Options', $this->text_domain ) ) ?></h2>
    <form method="post" action="options.php">
    	<?php settings_fields( 'ten321-theme-options-group' ) ?>
    	<?php do_settings_sections( 'ten321-options' ) ?>
        <p><input type="submit" value="<?php _e( 'Save', $this->text_domain ) ?>" class="button-primary"/></p>
    </form>
</div>
<?php
		}
		
		/**
		 * Sanitize options before committing to database
		 */
		function sanitize_options( $input ) {
			$fields = $this->_get_settings_fields() + $this->_get_blog_options_fields();
			$output = array();
			foreach( $input as $name=>$field ) {
				if( array_key_exists( $name, $fields ) ) {
					switch( $fields[$name]['type'] ) {
						case 'textarea':
							$output[$name] = wp_kses_post( $field );
						break;
						case 'text':
							$output[$name] = esc_attr( $field );
							break;
						default:
							$output[$name] = $field;
					}
				}
			}
			return $output;
		}
		
		/**
		 * Temporary fix for the fact that WordPress <=3.3.1 does not strip + or % chars from uploaded files
		 */
		function sanitize_file_name_chars( $chars ) {
			array_push( $chars, '+', '%' );
			return $chars;
		}
		
		/**
		 * Determine whether or not this is the blog archive (front_page)
		 * Adjust the query accordingly
		 */
		function front_page_query() {
			if ( ! is_home() )
				return;
			
			$excluded = $this->_get_options( 'exclude-cats', null );
			if ( empty( $excluded ) )
				return;
			
			$catstring = 'cat=-' . implode( ',-', $excluded );
			
			print( "\n<!-- Preparing to query posts with the following parameter:\n" );
			var_dump( $catstring );
			print( "\n-->\n" );
			
			query_posts( $catstring );
		}
		
		/**
		 * Output the post author and date
		 */
		function before_content() {
			if ( is_404() )
				return;
			
			global $post;
			if ( ! isset( $post ) || 'post' !== $post->post_type )
				return;
			
			$this->_get_options();
			$this->options['show-author'] = (int) $this->options['show-author'];
			$this->options['show-date'] = (int) $this->options['show-date'];
			
			if ( 0 === $this->options['show-author'] && 0 === $this->options['show-date'] )
				return;
			
			$post_meta = __( 'Posted ', $this->text_domain );
			if ( 0 !== $this->options['show-date'] )
				$post_meta .= __( ' on <span class="post-date">%1$s</span>', $this->text_domain );
			if ( 0 !== $this->options['show-author'] )
				$post_meta .= __( ' by <span class="post-author"><a href="%2$s">%3$s</a></span>', $this->text_domain );
?>
				<section class="post-top-meta">
					<p><?php echo apply_filters( 'ten321-top-post-meta', sprintf( $post_meta, get_the_date(), get_author_posts_url( get_the_author_meta( 'ID' ) ), get_the_author() ) ) ?></p>
				</section>
<?php
		}
		
		/**
		 * Output the categories, tags and author meta
		 */
		function after_content() {
			if ( is_404() )
				return;
			
			global $post;
			if ( ! isset( $post ) || 'post' !== $post->post_type )
				return;
			
			$this->_get_options();
			$this->options['show-cats'] = (int) $this->options['show-cats'];
			$this->options['show-tags'] = (int) $this->options['show-tags'];
			$this->options['show-bio'] = (int) $this->options['show-bio'];
			
			$categories = get_the_category();
			$cats = array();
			if ( is_array( $categories ) ) {
				foreach ( $categories as $cat ) {
					$cats[] = '<a href="' . get_category_link( $cat->term_id ) . '">' . $cat->name . '</a>';
				}
			}
			
			$taglist = get_the_tags();
			$tags = array();
			if ( is_array( $taglist ) ) {
				foreach( $taglist as $t ) {
					$tags[] = '<a href="' . get_tag_link( $t->term_id ) . '">' . $t->name . '</a>';
				}
			}
			
			$post_meta = '';
			if ( 0 !== $this->options['show-cats'] && ! empty( $cats ) )
				$post_meta .= __( '<p class="post-categories">Posted in: %1$s</p>', $this->text_domain );
			if ( 0 !== $this->options['show-tags'] && ! empty( $tags ) )
				$post_meta .= __( '<p class="post-tags">Tagged with: %2$s</p>', $this->text_domain );
?>
				<section class="post-bottom-meta">
					<?php echo apply_filters( 'ten321-bottom-post-meta', sprintf( $post_meta, implode( ', ', $cats ), implode( ', ', $tags ) ) ); ?>
				</section>
<?php
			if ( ! is_singular() )
				return;
			
			if ( 0 !== $this->options['show-bio'] && $bio = get_the_author_meta( 'description' ) ) {
?>
				<article class="post-author-meta">
					<figure class="alignleft"><?php echo get_avatar( get_the_author_meta('user_email'), 125 ) ?></figure>
					<?php echo $bio ?>
                    <br class="clear"/>
				</article>
<?php
			}
		}
		
		/**
		 * Output comment
		 */
		function comment_list( $comment, $args, $depth ) {
			$GLOBALS['comment'] = $comment;
			switch ( $comment->comment_type ) :
				case 'pingback' :
				case 'trackback' :
			?>
			<li class="post pingback">
				<p><?php _e( 'Pingback:', 'twentyeleven' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( 'Edit', 'twentyeleven' ), '<span class="edit-link">', '</span>' ); ?></p>
			<?php
					break;
				default :
			?>
			<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
				<article id="comment-<?php comment_ID(); ?>" class="comment">
					<footer class="comment-meta">
						<div class="comment-author vcard">
							<?php
								$avatar_size = 68;
								if ( '0' != $comment->comment_parent )
									$avatar_size = 39;
		
								echo get_avatar( $comment, $avatar_size );
		
								/* translators: 1: comment author, 2: date and time */
								printf( __( '%1$s on %2$s <span class="says">said:</span>', 'twentyeleven' ),
									sprintf( '<span class="fn">%s</span>', get_comment_author_link() ),
									sprintf( '<a href="%1$s"><time pubdate datetime="%2$s">%3$s</time></a>',
										esc_url( get_comment_link( $comment->comment_ID ) ),
										get_comment_time( 'c' ),
										/* translators: 1: date, 2: time */
										sprintf( __( '%1$s at %2$s', 'twentyeleven' ), get_comment_date(), get_comment_time() )
									)
								);
							?>
		
							<?php edit_comment_link( __( 'Edit', 'twentyeleven' ), '<span class="edit-link">', '</span>' ); ?>
						</div><!-- .comment-author .vcard -->
		
						<?php if ( $comment->comment_approved == '0' ) : ?>
							<em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'twentyeleven' ); ?></em>
							<br />
						<?php endif; ?>
		
					</footer>
		
					<div class="comment-content"><?php comment_text(); ?></div>
		
					<div class="reply">
						<?php comment_reply_link( array_merge( $args, array( 'reply_text' => __( 'Reply <span>&darr;</span>', 'twentyeleven' ), 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
					</div><!-- .reply -->
				</article><!-- #comment-## -->
		
			<?php
					break;
			endswitch;
		}
		
		/**
		 * Pagination
		 */
		function ten321_pagination() {
			if ( is_singular() ) {
				if ( function_exists( 'wp_pagenavi' ) ) {
					wp_pagenavi( array( 'type' => 'multipart' ) );
					return;
				}
				wp_link_pages();
				return;
			}
			
			if ( function_exists( 'wp_pagenavi' ) ) {
				wp_pagenavi();
				return;
			}
			
?>
<div class="page-links">
    <div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', $this->text_domain ) ); ?></div>
    <div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', $this->text_domain ) ); ?></div>
</div>
<?php
		}
	} /* ten321_framework class */
} /* if class exists */

add_action( 'after_setup_theme', 'init_ten321_framework' );
if( !function_exists( 'init_ten321_framework' ) ) {
	function init_ten321_framework() {
		global $ten321;
		$ten321 = new ten321_framework;
	}
}
?>