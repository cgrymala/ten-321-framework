<?php global $ten321; ?>
<?php
/**
 * Outputs the footer navigation custom menu, if that menu has been defined
 * @uses apply_filters() to apply the ten321-nav-args filter to the args array
 * 		for the wp_nav_menu() function. That filter sends the array as the first
 * 		param and the string 'footer-nav' as the second param
 */
if( function_exists( 'wp_nav_menu' ) && apply_filters( 'ten321_has_nav_menu', has_nav_menu( 'footer-nav' ), 'footer-nav' ) ) {
	wp_nav_menu( apply_filters( 'ten321-nav-args', array(
		'theme_location' => 'footer-nav',
        'container' => 'nav',
		'container_class' => 'footer-nav',
        'menu_id' => 'footer-nav',
		'fallback_cb' => array( $ten321, 'ten321_footernav_fallback' ),
	), 'footer-nav' ) );
}
?>